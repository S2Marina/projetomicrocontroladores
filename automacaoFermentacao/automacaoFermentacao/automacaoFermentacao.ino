#include <Time.h>
#include <TimeAlarms.h>

//todos os reles ligam em LOW e desligam e HIGH
//sim é estranho, mas estão ao contrario mesmo


// Carrega a biblioteca virtuabotixRTC
#include <virtuabotixRTC.h>           

// Determina os pinos ligados ao modulo
// myRTC(clock, data, rst)
virtuabotixRTC myRTC(A8, A9,A10);


int ledVermelho = 49;
int ledAzul = 51;
int ledAmarelo = 53;
int buzina = 47;
const byte ledPin = 13;
const byte interruptPin = 2;
const byte interruptPin2 = 3;
const byte interruptPin3 = 18;
const byte interruptPin4 = 19;
const byte interruptPin5 = 20;
const byte interruptPin6 = 21;

volatile byte state = LOW;

//Coolers e ponte H
int releCoolerInterno = 12;
int releCoolerExterno = 8; 
int relePeltier = 11;
int releEsq = 9;
int releDir = 10;

//Variáveis de tempo
volatile int hora = 0;
volatile int minuto = 0;
volatile int horaRestante = 0;
volatile int minutoRestante = 0;
int m = 0;
int h = 0;

//Variáveis de controle
int salvaTempo = 0;
int funcao = 0;
int contagem = 0;
int controle = 0;
int modo = 0;
int desliguei = 0;

volatile int counter = 0;
#include <LiquidCrystal.h> //Biblioteca do LCD
//Sensores de temperatura e umidade
#include <dht.h>

#define dht_dpin A0 //Pino DATA do Sensor ligado na porta Analogica A0

dht DHT; //Inicializa o sensor
volatile float temperaturaInterior = 0;
volatile float umidadeInterior = 0;
//int LM35 = A5; // Define o pino que lera a saída do LM35
//float temperaturaExterna; // Variável que armazenará a temperatura medida

LiquidCrystal lcd(22, 24, 4, 5, 6, 7);

void setup() {
  lcd.begin(16, 2);
  pinMode(ledPin, OUTPUT);
  pinMode(ledAmarelo, OUTPUT);
  pinMode(ledAzul, OUTPUT);
  pinMode(ledVermelho, OUTPUT);

  pinMode(releCoolerInterno, OUTPUT);
  pinMode(releCoolerExterno, OUTPUT);
  pinMode(releEsq, OUTPUT);
  pinMode(releDir, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), botaoEsfriar, CHANGE);
  pinMode(interruptPin2, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin2), botaoIncrementa, CHANGE);
  pinMode(interruptPin3, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin3), botaoAquecer, CHANGE);
   pinMode(interruptPin4, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin4), botaoDecrementa, CHANGE);
  pinMode(interruptPin5, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin5), botaoSalvar, CHANGE);
  pinMode(interruptPin6, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin6), botaoAutomatico, CHANGE);
  lcd.print("SELECIONE FUNCAO ");
  lcd.setCursor(0, 1);
  lcd.print("ABAIXO C/ BOTOES ");   
  // Informacoes iniciais de data e hora
  // Apos setar as informacoes, comente a linha abaixo
  // (segundos, minutos, hora, dia da semana, dia do mes, mes, ano)
 // myRTC.setDS1302Time(00, 19, 10, 6, 14, 12, 2018);
  desliga();
  pinMode(buzina,OUTPUT);
}

void loop() {
  //Não convem chamar uma mensagem aqui, pois acaba sobrescrevendo tudo e fica muito estranho o lcd não dando para limpar!
  myRTC.updateTime(); 
        switch(modo)
        {
          case 1:
                ///aquecer();
                if((hora != myRTC.hours || hora == myRTC.hours) && (minuto != myRTC.minutes)  )
                {
                  
                  DHT.read11(dht_dpin);
                  delay(2000);
               //   Alarm.delay(2000);
                  lcd.clear();
                  lcd.print("T=");
                  lcd.print(DHT.temperature);
                  horas();
                  horaFinal();
                  temperaturaInterior =  DHT.temperature;
                  if(temperaturaInterior < 34)
                  {
                  digitalWrite(ledVermelho, HIGH);
                  digitalWrite(releDir, HIGH);
                  digitalWrite(releEsq, LOW); //liga
                  digitalWrite(releCoolerInterno, LOW); //liga
                  }
                  else{//pão fica muito seco (perde consistencia)
                   digitalWrite(ledVermelho, HIGH);
                   delay(1000);
                   digitalWrite(ledVermelho, LOW);
                   digitalWrite(releDir, HIGH);
                   digitalWrite(releEsq,HIGH); //desliga
                   digitalWrite(releCoolerInterno, HIGH); //desliga
                   digitalWrite(releCoolerExterno, HIGH);
                  }
                }
                else{
                   tocaBuzina();
                   digitalWrite(ledVermelho, LOW);
                   digitalWrite(releDir, HIGH);
                   digitalWrite(releEsq,HIGH); //desliga
                   digitalWrite(releCoolerInterno, HIGH); //desliga
                   digitalWrite(releCoolerExterno, HIGH);
                   modo = 5;
                   salvaTempo = 0;
                   funcao = 0;
                   temperaturaInterior = 0;
                }
          break;
          case 2:
               // esfriar();
                if((hora != myRTC.hours || hora == myRTC.hours) && (minuto != myRTC.minutes)  )
                {
                  DHT.read11(dht_dpin);
                  delay(2000);
               //   Alarm.delay(2000);
                  lcd.clear();
                  lcd.print(DHT.temperature);
                  horas();
                  horaFinal();
                  temperaturaInterior =  DHT.temperature;
                 if(temperaturaInterior > 24) //é o minimo que chega em um periodo de pouco tempo funcionando
                 {
                 digitalWrite(ledAzul,HIGH);  
                 digitalWrite(releEsq,HIGH);
                 digitalWrite(releDir, LOW);//LIGANDO
                 digitalWrite(releCoolerInterno, LOW);
                 digitalWrite(releCoolerExterno, LOW);
                 }
                 else{
                    digitalWrite(ledAzul, HIGH);
                    delay(1000);
                    digitalWrite(ledAzul, LOW);
                    digitalWrite(releDir, HIGH);
                    digitalWrite(releEsq,HIGH); //desliga
                    digitalWrite(releCoolerInterno, HIGH); //desliga
                    digitalWrite(releCoolerExterno, HIGH);
                 }
                }
                else{
                    tocaBuzina();
                    digitalWrite(ledAzul, LOW);
                    digitalWrite(releDir, HIGH);
                    digitalWrite(releEsq,HIGH); //desliga
                    digitalWrite(releCoolerInterno, HIGH); //desliga
                    digitalWrite(releCoolerExterno, HIGH);
                   salvaTempo = 0;
                   funcao = 0;
                   modo = 5;
                }
          break;
          case 3: //modo automatico
            //Primeiro modo é esfriar que resulta em um retardamento da fermentação
              horaRestante = hora - 1;//lembro que ele resfria até esse momento 
              minutoRestante = minuto - 30;               //serve para isso também
                if((horaRestante != myRTC.hours || horaRestante == myRTC.hours) && (minutoRestante != myRTC.minutes)  )
                {
                  DHT.read11(dht_dpin);
                  delay(2000);
               //   Alarm.delay(2000);
                  lcd.clear();
                  lcd.print(DHT.temperature);
                  horas();
                  horaFinal();
                  temperaturaInterior =  DHT.temperature;
                  if(temperaturaInterior > 24) //é o minimo que chega em um periodo de pouco tempo funcionando
                   {
                   digitalWrite(ledAzul,HIGH);  
                   digitalWrite(releEsq,HIGH);
                   digitalWrite(releDir, LOW);//LIGANDO
                   digitalWrite(releCoolerInterno, LOW);
                   digitalWrite(releCoolerExterno, LOW);
                  }
                   else{
                      digitalWrite(ledAzul, HIGH);
                      delay(1000);
                      digitalWrite(ledAzul, LOW);
                      digitalWrite(releDir, HIGH);
                      digitalWrite(releEsq,HIGH); //desliga
                      digitalWrite(releCoolerInterno, HIGH); //desliga
                      digitalWrite(releCoolerExterno, HIGH);
                   }
                }
                else {
                    digitalWrite(ledAzul, LOW);
                    digitalWrite(releDir, HIGH);
                    digitalWrite(releEsq,HIGH); //desliga
                    digitalWrite(releCoolerInterno, HIGH); //desliga
                    digitalWrite(releCoolerExterno, HIGH);
                    modo = 4;
                    //Preciso fazer o pão crescer agora 
                } 
           break;
           case 4:
                  //já passei pelo periodo anterior
                        if((hora != myRTC.hours || hora == myRTC.hours) && (minuto != myRTC.minutes) )
                        {
                          DHT.read11(dht_dpin);
                          delay(2000);
                          lcd.clear();
                          lcd.print("T=");
                          lcd.print(DHT.temperature);
                          horas();
                          horaFinal();
                          temperaturaInterior =  DHT.temperature;
                          if(temperaturaInterior < 34)
                          {
                          digitalWrite(ledVermelho, HIGH);
                          digitalWrite(releDir, HIGH);
                          digitalWrite(releEsq, LOW); //liga
                          digitalWrite(releCoolerInterno, LOW); //liga
                          }
                          else{//pão fica muito seco (perde consistencia)
                           digitalWrite(ledVermelho, HIGH);
                           delay(1000);
                           digitalWrite(ledVermelho, LOW);
                           digitalWrite(releDir, HIGH);
                           digitalWrite(releEsq,HIGH); //desliga
                           digitalWrite(releCoolerInterno, HIGH); //desliga
                           digitalWrite(releCoolerExterno, HIGH);
                          }
                        }
                        else{
                           tocaBuzina();
                           digitalWrite(ledVermelho, LOW);
                           digitalWrite(ledAmarelo ,LOW);
                           digitalWrite(releDir, HIGH);
                           digitalWrite(releEsq,HIGH); //desliga
                           digitalWrite(releCoolerInterno, HIGH); //desliga
                           digitalWrite(releCoolerExterno, HIGH);
                           modo = 5;
                           salvaTempo = 0;
                           funcao = 0;
                           temperaturaInterior = 0;
                        }
          break;
          case 5:
                   lcd.clear();
                   lcd.print("SELECIONE FUNCAO ");
                   lcd.setCursor(0, 1);
                   lcd.print("ABAIXO C/ BOTOES ");   
          break;
        }
}


void data()
{
  myRTC.updateTime(); 
      // Imprime as informacoes no lcd
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Data : ");
  // Chama a rotina que imprime o dia da semana
  imprime_dia_da_semana(myRTC.dayofweek);
  //Serial.print(", ");
  lcd.print(myRTC.dayofmonth);//dia do mes
  lcd.print("/");
  lcd.print(myRTC.month);//qual mês
  lcd.print("/");
  lcd.print(myRTC.year);//ano
 // Serial.print("  ");
 
  delay( 1000);
}
void horas()
{
  myRTC.updateTime(); 
 // lcd.clear();
  lcd.setCursor(0,2);
  lcd.print("ATUAL=");
  // Adiciona um 0 caso o valor da hora seja <10
  if (myRTC.hours < 10)
  {
    lcd.print("0");
  }
  lcd.print(myRTC.hours); //retorna hora atual
  lcd.print("h");
  // Adiciona um 0 caso o valor dos minutos seja <10
  if (myRTC.minutes < 10)
  {
   lcd.print("0");
  }
  lcd.print(myRTC.minutes); //retorna minuto atual
  lcd.print("min");
  // Adiciona um 0 caso o valor dos segundos seja <10
 // if (myRTC.seconds < 10)
 // {
  // lcd.print("0");
//  }
 // lcd.println(myRTC.seconds);
  lcd.println("     ");
  delay(1000);
}

void imprime_dia_da_semana(int dia)
{
  switch (dia)
  {
    case 1:
     lcd.print("Domingo");
    break;
    case 2:
     lcd.print("Segunda");
    break;
    case 3:
    lcd.print("Terca");
    break;
    case 4:
  lcd.print("Quarta");
    break;
    case 5:
  lcd.print("Quinta");
    break;
    case 6:
  lcd.print("Sexta");
    break;
    case 7:
   lcd.print("Sabado");
    break;
  }
}

void botaoEsfriar() {//esfriar
//  state = !state;
//  lcd.clear();
//  lcd.setCursor(3, 0);
//  lcd.print(" OI ESTA BEM");
    digitalWrite(ledAzul, HIGH);
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("SELECIONE TEMPO");
    funcao = 2;
}

void botaoIncrementa() { //up - incrementa
//  state = !state;
//  lcd.setCursor(3, 0);
//  lcd.print("TUDO SIM E TU");
//    delay(6000);
    if(funcao == 3)//precisa ser com 2 horas a mais de intervalo da hora atual entao setamos para o usuario ver 
    {
      hora = myRTC.hours + 2;
    }
    minuto = minuto + 1;
      if(minuto > 59)
      {
        hora = hora + 1;
        minuto = 0;
      }
      if(hora > 24)
      {
        hora = 1;
        minuto = 0;
      }
      lcd.clear();
      lcd.print("FIM=");
      lcd.print(hora);
      lcd.print("h");
      lcd.print(minuto);
      lcd.print("min ");
      horas();
      salvaTempo = 1;
}

void botaoAquecer() { //aquecer
//  state = !state;
//  lcd.setCursor(3, 0);
//  lcd.print("BEM                        ");
//    delay(6000);
  lcd.clear();
  digitalWrite(ledVermelho, HIGH);
  lcd.setCursor(0,0);
  lcd.print("SELECIONE TEMPO");
  funcao = 1;
}

void botaoDecrementa() {//decrementar
//  state = !state;
//  lcd.setCursor(3, 0);
//  lcd.print("E A FACULDADE                                " );
//  delay(6000);
      minuto = minuto - 1;
      if(minuto < 0)
      {
        hora = hora - 1;
        minuto = 59;  
      }
      if(hora < 1)
      {
        hora = 24;
        minuto = 59;
      }
      lcd.clear();
      lcd.print("FIM=");
      lcd.print(hora);
      lcd.print("h");
      lcd.print(minuto);
      lcd.print("min");
      horas();
      salvaTempo = 1;
}

void botaoSalvar() { //ok
//  state = !state;
//  lcd.setCursor(3, 0);
//  lcd.print("VAI BEM OBG                      ");
//    delay(6000);
    if(salvaTempo == 1)
    {
//      switch(funcao)
//        {
//          case 1:
//                aquecer();
//          break;
//          case 2:
//                esfriar();
//          break;
//        }
      h = hora;
      m = minuto;
      modo = funcao;
//      lcd.clear();
  //    lcd.print("     aaa");
      loop();
    }
    else{
      //mensagem de selecionar uma opção através dos botões
      lcd.clear();
      lcd.print("SELECIONE FUNCAO ");
      lcd.setCursor(0, 1);
      lcd.print("ABAIXO C/ BOTOES ");    
      funcao = 0;
      salvaTempo = 0;
    }
}

void botaoAutomatico() //automatico
{
 lcd.clear();
  digitalWrite(ledAmarelo, HIGH);
  lcd.setCursor(0,0);
  lcd.print("SELECIONE TEMPO");
  funcao = 3;
}

void desliga()
{
    digitalWrite(releDir, HIGH);
    digitalWrite(releEsq,HIGH); //desliga
    digitalWrite(releCoolerInterno, HIGH); //desliga
    digitalWrite(releCoolerExterno, HIGH);
}


void horaFinal(){
  lcd.setCursor(8, 0);
  lcd.print(hora);
  lcd.print("h");
  lcd.print(minuto);
  lcd.print("min");
    delay(1000);
}

void tocaBuzina()
{
  //Ligando o buzzer com uma frequencia de 1500 hz.
  tone(buzina,2250);   
  delay(500);
   
  //Desligando o buzzer.
  noTone(buzina);
  delay(500);  
}
